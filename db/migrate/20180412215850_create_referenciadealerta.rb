class CreateReferenciadealerta < ActiveRecord::Migration[5.0]
  def change
    create_table :referenciadealerta do |t|
      t.integer :idAlerta
      t.integer :idCampo
      t.string :campo
      t.string :valor

      t.timestamps
    end
  end
end
