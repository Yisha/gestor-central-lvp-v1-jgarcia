class CreatePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :permissions do |t|
      t.integer :view_id
      t.string :view_name
      t.integer :crear
      t.integer :editar
      t.integer :leer
      t.integer :eliminar
      t.integer :profile_id

      t.timestamps
    end
  end
end
