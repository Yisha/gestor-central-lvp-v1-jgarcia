//= require iCheck/icheck.min.js

console.log('Soy alertn');

$(document).on('click', '.linkTran', function(){
   alert('Moviiii link')
    var metodo = $("#metodo").val()
    let obseArea = $('#obseArea').val(),
        estado = $('#state_ob').val(),
        id = $(this).html();
    alert(id)

    //Traducciones
    let titleAlert = $('#titleAlert').val(),
        datosAlert = $('#datosAlert').val(),
        loseData = $('#loseData').val(),
        btn_continue = $('#btn_continue').val(),
        btn_yes = $('#btn_yes').val(),
        btn_cancel = $('#btn_cancel').val();



    if( obseArea != "" || estado != "" ){
        swal({
                title: `${titleAlert}`,
                text: `${datosAlert}`,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                // confirmButtonText: `${btn_yes}`,
                confirmButtonText: `${btn_continue}`,
                // cancelButtonText: `${btn_cancel}`,
                cancelButtonText: `${btn_cancel}`,
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {

                if (isConfirm){

                    swal({
                            title: `${titleAlert}`,
                            // text: `${yourSure}`,
                            text: `${loseData}`,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            // confirmButtonText: `${btn_confir}`,
                            confirmButtonText: `${btn_continue}`,
                            // cancelButtonText: `${btn_cancel2}`,
                            cancelButtonText: `${btn_cancel}`,
                            closeOnConfirm: false,
                            closeOnCancel: false
                        },
                        function (isConfirm) {

                            if (isConfirm){

                                // Se envia el ajax para eliminar el filtro
                                // window.location.href = `/tx_filters/${id}/edit`

                                window.location.href = `/alertn/${metodo}?id=${id}`

                            }
                            else{
                                swal(`Cancelado`,`Tus datos estan seguros`, "error");
                            }
                        });

                } else {
                    swal(`Cancelado`,`Tus datos estan seguros`, "error");
                }
            });
    } else {
        window.location.href = `/alertn/${metodo}?id=${id}`
    }

});

// -- Eviar observación de modal
$('#saveObse').on('click', function(){
    // alert('Voy a guardar Observacion')

    let idH = $('#idH').val(),
        id_tranH = $('#id_tranH').val(),
        idTranH = $('#idTranH').val(),
        historicoH = $('#valor').val(),
        obseArea = $('#obseArea').val(),
        state_ob = $('#state_ob').val(),
        numTarj = $('#numTarj').val(), //Este valor es para la parcial de de historial de alertas
        apply_obse = $('#apply_obse').val(),
        fraudRadios = $('#fraud_radio').find('input[name=fraud]'),
        fraudH,
        $_this_input_chekeado;

    fraudRadios.each(function(){
        $_this_input_chekeado = $(this);

        if ( $_this_input_chekeado.prop('checked') ){
            fraudH = $_this_input_chekeado.val();
            // alert(`Estoy chekeado ${$_this_input_chekeado.attr('id')} mi valor es ${$_this_input_chekeado.val()} y farud = ${fraudH}`);
        }
    });

    // alert(`Estoy voy agregar por AJAX ${idH}, ${id_tranH} ${historicoH} ${obseArea} IdEstado${state_ob}`);

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: '../alertn/creacion_observaciones',
        data: {
            // "_method": "observaciones",
            //--- Tbitacora
            idTbitacora: idH,
            id_tran: id_tranH,
            idTran: idTranH,
            historico: historicoH,
            observaciones: obseArea,
            estado: state_ob,
            apply: apply_obse,
            fraud: fraudH
        },
        success: function(result, status, xhr){
            //-----Eliminar valores de historico
            $('#valor').val('');
            console.log('entre load ........' + result + status + xhr);
            //--- Recargarndo parcial

            $('#observacionesTable').load(`../alertn/observaciones?idTbitacora=${idH}`);

            //---- Recet Valores en modal agregar observaciones
            $('#obseArea').val('');
            $('#state_ob').val('');
            $('#fraud_radio').find('div.checked').removeClass('checked');
            $_this_input_chekeado.prop('checked', false);

            if( historicoH != ''){
                // $('#histAlertasTab').load(`/alertn/historialAlertas?id=${idH}&IdTran=${numTarj}`);
                window.location.href = `/alertn/alertTran?id=${idH}`
            }

            swal({
                position: 'top-end',
                type: 'success',
                title: 'Ok',
                text: '¡Observación agregada!',
                showConfirmButton: true,
                timer: 3000
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {

            swal({
                position: 'top-end',
                type: 'error',
                title: '¡Error!',
                text: `Error ${xhr.status} en ${thrownError}`,
                showConfirmButton: true,
                timer: 5000
            });
        }
    });
});


//--- desde nueva alerta
$('#saveObseNueva').on('click', function(){
    // alert('Voy a guardar Observacion')

    let idH = $('#idH').val(),
        id_tranH = $('#id_tranH').val(),
        idTranH = $('#idTranH').val(),
        historicoH = $('#valor').val(),
        obseArea = $('#obseArea').val(),
        state_ob = $('#state_ob').val(),
        numTarj = $('#numTarj').val(), //Este valor es para la parcial de de historial de alertas
        apply_obse = $('#apply_obse').val(),
        fraudRadios = $('#fraud_radio').find('input[name=fraud]'),
        fraudH,
        $_this_input_chekeado;

    fraudRadios.each(function(){
        $_this_input_chekeado = $(this);

        if ( $_this_input_chekeado.prop('checked') ){
            fraudH = $_this_input_chekeado.val();
            // alert(`Estoy chekeado ${$_this_input_chekeado.attr('id')} mi valor es ${$_this_input_chekeado.val()} y farud = ${fraudH}`);
        }
    });

    // alert(`Estoy voy agregar por AJAX ID:${idH}, IdTran:${id_tranH} Historico:${historicoH} :Observacion${obseArea} IdEstado${state_ob}`);

    $.ajax({
        type: "POST",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
        },
        url: '../alertn/creacion_observaciones',
        data: {
            // "_method": "observaciones",
            //--- Tbitacora
            idTbitacora: idH,
            id_tran: id_tranH,
            idTran: idTranH,
            historico: historicoH,
            observaciones: obseArea,
            estado: state_ob,
            apply: apply_obse,
            fraud: fraudH
        },
        success: function(result, status, xhr){
            //-----Eliminar valores de historico
            // $('#valor').val('');
            console.log('entre load ........' + result + status + xhr);
            //--- Recargarndo parcial
            //
            // $('#observacionesTable').load(`/alertn/observaciones?idTbitacora=${idH}`);
            //
            // //---- Recet Valores en modal agregar observaciones
            // $('#obseArea').val('');
            // $('#state_ob').val('');
            // $('#fraud_radio').find('div.checked').removeClass('checked');
            // $_this_input_chekeado.prop('checked', false);
            //
            // if( historicoH != ''){
                // $('#histAlertasTab').load(`/alertn/historialAlertas?id=${idH}&IdTran=${numTarj}`);
            swal({
                position: 'top-end',
                type: 'success',
                title: 'Ok',
                text: '¡Observación agregada!',
                showConfirmButton: true,
                timer: 3000
            });

            window.location.href = `../alertn/alertTran?id=${idH}`
            // }

        },
        error: function (xhr, ajaxOptions, thrownError) {

            swal({
                position: 'top-end',
                type: 'error',
                title: '¡Error!',
                text: `Error ${xhr.status} en ${thrownError}`,
                showConfirmButton: true,
                timer: 5000
            });
        }
    });
});

let btn_addList = document.getElementById('addWhiteList');

btn_addList.addEventListener('click', function(){
  let IdTran =  document.getElementById("numTarj").value

    swal({
        title: `¿Estas Seguro?`,
        text: `Agregara este Número de tarjeta ${IdTran} a lista blanca`,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: `Continuar`,
        cancelButtonText: `Cancelar`,
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {

        if (isConfirm){

            swal({
                    title: `Confirmar`,
                    text: `Receptor central dejara de generar alertas de este número de tarjeta ${IdTran}`,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: `Continuar`,
                    cancelButtonText: `Cancelar`,
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {

                    if (isConfirm){

                        //---Agregando todos los filtros
                        $.ajax({
                            type: "POST",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                            },
                            url: '../alertn/agregar_a_lista_blanca',
                            data: { IdTran: IdTran },
                            success: function(data){

                                btn_addList.style.display = 'none';

                                swal({
                                    position: 'top-end',
                                    type: 'success',
                                    title: 'Ok',
                                    text: `Fue agregado exitosamente`,
                                    showConfirmButton: true,
                                    timer: 3000
                                });

                            }
                        });

                    }
                    else{

                        //Esto es para que no se chechee el input
                        // inputCheck.prop('checked', false);

                        swal({
                            position: 'top-end',
                            type: 'error',
                            title: 'Ok',
                            text: `No se realizo ninguna acción`,
                            showConfirmButton: true,
                            timer: 3000
                        });
                    }
                });

        } else {

            //Esto es para que no se chechee el input
            // inputCheck.prop('checked', false);

            swal({
                position: 'top-end',
                type: 'error',
                title: 'Ok',
                text: `No se realizo ninguna acción`,
                showConfirmButton: true,
                timer: 3000
            });
        }
    });
});
// ---------------------------------------------------------------------------------------------

let btn_hab = document.getElementById('habi');


btn_hab.addEventListener('click', function() {
    let ids = document.getElementById('idt').value;
    let msg = "Se Desbloqueara esta tarjeta";
    let msg2 = "La tarjeta sea Desbloqueado";



    swal({
            title: `¿Estas Seguro?`,
            text: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: `Continuar`,
            cancelButtonText: `Cancelar`,
            closeOnConfirm: false,
            closeOnCancel: false
        },

        function (isConfirm) {
        if (isConfirm) {

            $.ajax({
                type: "POST",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
                },
                url: '../alertn/bloqueo_de_cuenta',
                data: {idt: ids},
                success: function (data) {
                    if(typeof data === "string") {
                        swal({
                            position: 'top-end',
                            type: 'warning',
                            title: 'OK',
                            text: data,
                            showConfirmButton: true,
                        });

                    }else{
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'OK',
                            text: msg2,
                            showConfirmButton: true,
                            timer: 3000
                        });
                        btn_hab.style.display= "none";
                    }



                }
            });
        }else{

            //Esto es para que no se chechee el input
            // inputCheck.prop('checked', false);

            swal({
                position: 'top-end',
                type: 'error',
                title: 'Ok',
                text: `No se realizo ninguna acción`,
                showConfirmButton: true,
                timer: 3000
            });
        }
        });
        });

// let idtran = $('#idTranT').val(),
//     id_tranH = $('#id_tranH').val(),
//     idT;

// if( idtran != ''){
//     idT = idtran
// } else if ( id_tranH != ''){
//     idT = id_tranH
// }
//
// setInterval(function () {
//
//     console.log(idT);
//
//     parcialObse.load(`../alertn/observaciones?id=${idT}`, function(response, status, xhr){
//         if ( status === "error"){
//             var msg = "¡Ooops... hubo un error al recargar la tabla!";
//             $("#error").html(msg + xhr.status + " " + xhr.statusText);
//         }
//     });
// }, 2000);


//--- Codigo ajax Jose Luis Tabla Observaciones
// $(function(){
//     $(".pagination a").on("click", function(){
//         alert('Click en Kaminari')
//         $.get(this.href, null, null, "script");
//         return false;
//     })
// });
//--- Termina Codigo ajax Jose Luis Tabla Observaciones