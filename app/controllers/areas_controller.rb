class AreasController < ApplicationController
  before_action :set_area, only: [:show, :edit, :update, :destroy]

  @@byArea = "Areas"
  # GET /areas
  # GET /areas.json
  def index
    if current_user
      @page = "Areas"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "3"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      @date = Time.now
      # @description = General.all.count.to_i

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.description = @description
      # @historic.save


      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Areas' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearArea = permisos.crear
          @editarArea = permisos.editar
          @leerArea = permisos.leer
          @eliminarArea = permisos.eliminar

          if permisos.view_name == @@byArea

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

            @area_usuario = current_user.area_id

            if @area_usuario == "1"
              @areas = Area.all.page(params[:page]).per(10)

              areas_arr = Array.new

              @areas.each do |ar|
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = ar.id
                valor[:name] = ar.name
                valor[:area_id] = ar.area_id

                areas_arr.push(valor)
              end

              @areas = Kaminari.paginate_array(areas_arr).page(params[:page]).per(10)
            else
              @areas = Area.where("id = ?", @area_usuario)

              areas_arr = Array.new

              @areas.each do |ar|
                @nombre = ar.name
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = ar.id
                valor[:name] = ar.name
                valor[:area_id] = ar.area_id

                areas_arr.push(valor)

                @subarea_1 = Area.where("area_id = ?", ar.id)

                if !@subarea_1.nil?

                  @subarea_1.each do |sub|
                    valor = {id: '', name: '', area_id: ''}
                    valor[:id] = sub.id
                    valor[:name] = sub.name
                    valor[:area_id] = sub.area_id

                    areas_arr.push(valor)

                    @subarea_2 = Area.where("area_id = ?", sub.id)

                    if !@subarea_2.nil?

                      @subarea_2.each do |sub2|
                        valor = {id: '', name: '', area_id: ''}
                        valor[:id] = sub2.id
                        valor[:name] = sub2.name
                        valor[:area_id] = sub2.area_id

                        areas_arr.push(valor)

                        @subarea_3 = Area.where("area_id = ?", sub2.id)

                        if !@subarea_3.nil?

                          @subarea_3.each do |sub3|
                            valor = {id: '', name: '', area_id: ''}
                            valor[:id] = sub3.id
                            valor[:name] = sub3.name
                            valor[:area_id] = sub3.area_id

                            areas_arr.push(valor)

                            @subarea_4 = Area.where("area_id = ?", sub3.id)

                            if !@subarea_4.nil?

                              @subarea_4.each do |sub4|
                                valor = {id: '', name: '', area_id: ''}
                                valor[:id] = sub4.id
                                valor[:name] = sub4.name
                                valor[:area_id] = sub4.area_id

                                areas_arr.push(valor)

                                @subarea_5 = Area.where("area_id = ?", sub4.id)

                                if !@subarea_5.nil?

                                  @subarea_5.each do |sub5|
                                    valor = {id: '', name: '', area_id: ''}
                                    valor[:id] = sub5.id
                                    valor[:name] = sub5.name
                                    valor[:area_id] = sub5.area_id

                                    areas_arr.push(valor)

                                    @subarea_6 = Area.where("area_id = ?", sub5.id)

                                    if !@subarea_6.nil?

                                      @subarea_6.each do |sub6|
                                        valor = {id: '', name: '', area_id: ''}
                                        valor[:id] = sub6.id
                                        valor[:name] = sub6.name
                                        valor[:area_id] = sub6.area_id

                                        areas_arr.push(valor)

                                        @subarea_7 = Area.where("area_id = ?", sub6.id)

                                        if !@subarea_7.nil?

                                          @subarea_7.each do |sub7|
                                            valor = {id: '', name: '', area_id: ''}
                                            valor[:id] = sub7.id
                                            valor[:name] = sub7.name
                                            valor[:area_id] = sub7.area_id

                                            areas_arr.push(valor)

                                            @subarea_8 = Area.where("area_id = ?", sub7.id)

                                            if !@subarea_8.nil?

                                              @subarea_8.each do |sub8|
                                                valor = {id: '', name: '', area_id: ''}
                                                valor[:id] = sub8.id
                                                valor[:name] = sub8.name
                                                valor[:area_id] = sub8.area_id

                                                areas_arr.push(valor)

                                                @subarea_9 = Area.where("area_id = ?", sub8.id)

                                                if !@subarea_9.nil?

                                                  @subarea_9.each do |sub9|
                                                    valor = {id: '', name: '', area_id: ''}
                                                    valor[:id] = sub9.id
                                                    valor[:name] = sub9.name
                                                    valor[:area_id] = sub9.area_id

                                                    areas_arr.push(valor)

                                                    @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                    if !@subarea_10.nil?

                                                      @subarea_10.each do |sub10|
                                                        valor = {id: '', name: '', area_id: ''}
                                                        valor[:id] = sub10.id
                                                        valor[:name] = sub10.name
                                                        valor[:area_id] = sub10.area_id

                                                        areas_arr.push(valor)
                                                      end
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end

              @areas = Kaminari.paginate_array(areas_arr).page(params[:page]).per(10)
            end
            if params[:search].present?
              @belongs = params[:search].to_s

              if @belongs == "Central Office" || @belongs == "Oficina Central" || @belongs == "Central Office\t" || @belongs == "Oficina Central\t" || @belongs == "Central Office\t\t" || @belongs == "Oficina Central\t\t" || @belongs == "central office" || @belongs == "oficina central" || @belongs == "central office\t" || @belongs == "oficina central\t" || @belongs == "central office\t\t" || @belongs == "oficina central\t\t"
                @belongs= nil
                @busco = "2"
              elsif @belongs == "Store" || @belongs == "Tienda" || @belongs == "Store\t" || @belongs == "Tienda\t" || @belongs == "Store\t\t" || @belongs == "Tienda\t\t" || @belongs == "store" || @belongs == "tienda" || @belongs == "store\t" || @belongs == "tienda\t" || @belongs == "store\t\t" || @belongs == "tienda\t\t"
                @belongs = "1"
                @busco = "1"
              end

              name = params[:search]
              name = name.gsub! /\t/, ''
              if name.nil?
                @na = params[:search].to_s
              else
                @na = name
              end

              if !Area.find_by_name(@na).nil?
                @areas = Area.where("name = ?", @na).page(params[:page]).per(10)
                @query = params[:search]
              elsif !Area.find_by_store_id(@belongs).nil?
                if @busco == "2"
                  @areas = Area.where("store_id IS NULL").page(params[:page]).per(10)
                  @query = params[:search]
                elsif @busco == "1"
                  @areas = Area.where("store_id = ?", @busco).page(params[:page]).per(10)
                  @query = params[:search]
                else
                  @error = t('all.error')
                  @areas = Area.all.page(params[:page]).per(10)
                end
              else
                @error = t('all.error')
                @query = params[:search]
                @areas = Area.all.page(params[:page]).per(10)
              end
            end


          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /areas/1
  # GET /areas/1.json
  def show
    if current_user
      @page = "Areas"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "5"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # @historic.save

      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Areas' and profile_id = ?", @cu)

        @per.each do |permisos|
          @leerArea = permisos.editar

          if permisos.view_name == @@byArea

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if ((@@leer == 2))

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /areas/new
  def new
    if current_user
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Areas' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarArea = permisos.editar

          if permisos.view_name == @@byArea

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if ((@@crear == 8))
            @area_usuario = current_user.area_id

            if @area_usuario == "1"

              areas_arr = Array.new

              @areas = Area.all

              @areas.each do |ar|
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = ar.id
                valor[:name] = ar.name
                valor[:area_id] = ar.area_id

                areas_arr.push(valor)
              end

              @areas = areas_arr
            else
              @areas = Area.where("id = ?", @area_usuario)

              areas_arr = Array.new

              @areas.each do |ar|
                @nombre = ar.name
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = ar.id
                valor[:name] = ar.name
                valor[:area_id] = ar.area_id

                areas_arr.push(valor)

                @subarea_1 = Area.where("area_id = ?", ar.id)

                if !@subarea_1.nil?

                  @subarea_1.each do |sub|
                    valor = {id: '', name: '', area_id: ''}
                    valor[:id] = sub.id
                    valor[:name] = sub.name
                    valor[:area_id] = sub.area_id

                    areas_arr.push(valor)

                    @subarea_2 = Area.where("area_id = ?", sub.id)

                    if !@subarea_2.nil?

                      @subarea_2.each do |sub2|
                        valor = {id: '', name: '', area_id: ''}
                        valor[:id] = sub2.id
                        valor[:name] = sub2.name
                        valor[:area_id] = sub2.area_id

                        areas_arr.push(valor)

                        @subarea_3 = Area.where("area_id = ?", sub2.id)

                        if !@subarea_3.nil?

                          @subarea_3.each do |sub3|
                            valor = {id: '', name: '', area_id: ''}
                            valor[:id] = sub3.id
                            valor[:name] = sub3.name
                            valor[:area_id] = sub3.area_id

                            areas_arr.push(valor)

                            @subarea_4 = Area.where("area_id = ?", sub3.id)

                            if !@subarea_4.nil?

                              @subarea_4.each do |sub4|
                                valor = {id: '', name: '', area_id: ''}
                                valor[:id] = sub4.id
                                valor[:name] = sub4.name
                                valor[:area_id] = sub4.area_id

                                areas_arr.push(valor)

                                @subarea_5 = Area.where("area_id = ?", sub4.id)

                                if !@subarea_5.nil?

                                  @subarea_5.each do |sub5|
                                    valor = {id: '', name: '', area_id: ''}
                                    valor[:id] = sub5.id
                                    valor[:name] = sub5.name
                                    valor[:area_id] = sub5.area_id

                                    areas_arr.push(valor)

                                    @subarea_6 = Area.where("area_id = ?", sub5.id)

                                    if !@subarea_6.nil?

                                      @subarea_6.each do |sub6|
                                        valor = {id: '', name: '', area_id: ''}
                                        valor[:id] = sub6.id
                                        valor[:name] = sub6.name
                                        valor[:area_id] = sub6.area_id

                                        areas_arr.push(valor)

                                        @subarea_7 = Area.where("area_id = ?", sub6.id)

                                        if !@subarea_7.nil?

                                          @subarea_7.each do |sub7|
                                            valor = {id: '', name: '', area_id: ''}
                                            valor[:id] = sub7.id
                                            valor[:name] = sub7.name
                                            valor[:area_id] = sub7.area_id

                                            areas_arr.push(valor)

                                            @subarea_8 = Area.where("area_id = ?", sub7.id)

                                            if !@subarea_8.nil?

                                              @subarea_8.each do |sub8|
                                                valor = {id: '', name: '', area_id: ''}
                                                valor[:id] = sub8.id
                                                valor[:name] = sub8.name
                                                valor[:area_id] = sub8.area_id

                                                areas_arr.push(valor)

                                                @subarea_9 = Area.where("area_id = ?", sub8.id)

                                                if !@subarea_9.nil?

                                                  @subarea_9.each do |sub9|
                                                    valor = {id: '', name: '', area_id: ''}
                                                    valor[:id] = sub9.id
                                                    valor[:name] = sub9.name
                                                    valor[:area_id] = sub9.area_id

                                                    areas_arr.push(valor)

                                                    @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                    if !@subarea_10.nil?

                                                      @subarea_10.each do |sub10|
                                                        valor = {id: '', name: '', area_id: ''}
                                                        valor[:id] = sub10.id
                                                        valor[:name] = sub10.name
                                                        valor[:area_id] = sub10.area_id

                                                        areas_arr.push(valor)
                                                      end
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end

              @areas = areas_arr
            end

            @area = Area.new

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /areas/1/edit
  def edit
    if current_user
      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Areas' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarArea = permisos.editar

          if permisos.view_name == @@byArea

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if ((@@editar == 4) && (@area.id != 1))

            @area_usuario = current_user.area_id

            if @area_usuario == "1"

              areas_arr = Array.new

              @areas = Area.all

              @areas.each do |ar|
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = ar.id
                valor[:name] = ar.name
                valor[:area_id] = ar.area_id

                areas_arr.push(valor)
              end

              @areas = areas_arr
            else
              @areas = Area.where("id = ?", @area_usuario)

              areas_arr = Array.new

              @areas.each do |ar|
                @nombre = ar.name
                valor = {id: '', name: '', area_id: ''}
                valor[:id] = ar.id
                valor[:name] = ar.name
                valor[:area_id] = ar.area_id

                areas_arr.push(valor)

                @subarea_1 = Area.where("area_id = ?", ar.id)

                if !@subarea_1.nil?

                  @subarea_1.each do |sub|
                    valor = {id: '', name: '', area_id: ''}
                    valor[:id] = sub.id
                    valor[:name] = sub.name
                    valor[:area_id] = sub.area_id

                    areas_arr.push(valor)

                    @subarea_2 = Area.where("area_id = ?", sub.id)

                    if !@subarea_2.nil?

                      @subarea_2.each do |sub2|
                        valor = {id: '', name: '', area_id: ''}
                        valor[:id] = sub2.id
                        valor[:name] = sub2.name
                        valor[:area_id] = sub2.area_id

                        areas_arr.push(valor)

                        @subarea_3 = Area.where("area_id = ?", sub2.id)

                        if !@subarea_3.nil?

                          @subarea_3.each do |sub3|
                            valor = {id: '', name: '', area_id: ''}
                            valor[:id] = sub3.id
                            valor[:name] = sub3.name
                            valor[:area_id] = sub3.area_id

                            areas_arr.push(valor)

                            @subarea_4 = Area.where("area_id = ?", sub3.id)

                            if !@subarea_4.nil?

                              @subarea_4.each do |sub4|
                                valor = {id: '', name: '', area_id: ''}
                                valor[:id] = sub4.id
                                valor[:name] = sub4.name
                                valor[:area_id] = sub4.area_id

                                areas_arr.push(valor)

                                @subarea_5 = Area.where("area_id = ?", sub4.id)

                                if !@subarea_5.nil?

                                  @subarea_5.each do |sub5|
                                    valor = {id: '', name: '', area_id: ''}
                                    valor[:id] = sub5.id
                                    valor[:name] = sub5.name
                                    valor[:area_id] = sub5.area_id

                                    areas_arr.push(valor)

                                    @subarea_6 = Area.where("area_id = ?", sub5.id)

                                    if !@subarea_6.nil?

                                      @subarea_6.each do |sub6|
                                        valor = {id: '', name: '', area_id: ''}
                                        valor[:id] = sub6.id
                                        valor[:name] = sub6.name
                                        valor[:area_id] = sub6.area_id

                                        areas_arr.push(valor)

                                        @subarea_7 = Area.where("area_id = ?", sub6.id)

                                        if !@subarea_7.nil?

                                          @subarea_7.each do |sub7|
                                            valor = {id: '', name: '', area_id: ''}
                                            valor[:id] = sub7.id
                                            valor[:name] = sub7.name
                                            valor[:area_id] = sub7.area_id

                                            areas_arr.push(valor)

                                            @subarea_8 = Area.where("area_id = ?", sub7.id)

                                            if !@subarea_8.nil?

                                              @subarea_8.each do |sub8|
                                                valor = {id: '', name: '', area_id: ''}
                                                valor[:id] = sub8.id
                                                valor[:name] = sub8.name
                                                valor[:area_id] = sub8.area_id

                                                areas_arr.push(valor)

                                                @subarea_9 = Area.where("area_id = ?", sub8.id)

                                                if !@subarea_9.nil?

                                                  @subarea_9.each do |sub9|
                                                    valor = {id: '', name: '', area_id: ''}
                                                    valor[:id] = sub9.id
                                                    valor[:name] = sub9.name
                                                    valor[:area_id] = sub9.area_id

                                                    areas_arr.push(valor)

                                                    @subarea_10 = Area.where("area_id = ?", sub9.id)

                                                    if !@subarea_10.nil?

                                                      @subarea_10.each do |sub10|
                                                        valor = {id: '', name: '', area_id: ''}
                                                        valor[:id] = sub10.id
                                                        valor[:name] = sub10.name
                                                        valor[:area_id] = sub10.area_id

                                                        areas_arr.push(valor)
                                                      end
                                                    end
                                                  end
                                                end
                                              end
                                            end
                                          end
                                        end
                                      end
                                    end
                                  end
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                end
              end

              @areas = areas_arr
            end
          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # POST /areas
  # POST /areas.json
  def create
    if current_user
      @page = "Areas"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "1"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date


      @nombre = params[:area][:name]

      @existe = Area.find_by_name(@nombre.to_s)

      if @existe.nil?
        @area = Area.new(area_params)

        respond_to do |format|
          if @area.save
            # @historic.detail = @area.to_json
            # @historic.save

            format.html {redirect_to areas_path, notice: t('activerecord.atributes.ar')}
            #Genera el perfil de Administrador
            @perfil = Profile.new
            @perfil.name = ("Administrator " + @area.name)
            @perfil.area_id = @area.id
            @perfil.flag = 1
            @perfil.save

            @views = View.where.not(:view_name => 'Views').order(:id)

            @views.each do |vw|
              @pm = Permission.new
              @pm.view_id = vw.id
              @pm.view_name = vw.id
              @pm.profile_id = @perfil.id
              @pm.save
            end

            #Genera el perfil de Default
            @perfil = Profile.new
            @perfil.name = ("Default" + @area.name)
            @perfil.area_id = @area.id
            @perfil.flag = 2
            @perfil.save

            @views = View.where.not(:view_name => 'Views').order(:id)

            @views.each do |vw|
              @pm = Permission.new
              @pm.view_id = vw.id
              @pm.view_name = vw.id
              @pm.profile_id = @perfil.id
              @pm.save
            end

            format.html {redirect_to areas_path, notice: t('activerecord.atributes.ar')}
            format.json {render action: 'index', status: :created, location: @area}
          else
            format.html {render action: 'new'}
            format.json {render json: @area.errors, status: :unprocessable_entity}
          end
        end
      else
        redirect_to areas_path, alert: t('activerecord.atributes.not_saved')
      end

    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # PATCH/PUT /areas/1
  # PATCH/PUT /areas/1.json
  def update
    if current_user
      @page = "Areas"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "2"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # #@historic.save

      @nombre_antes = params[:nombrePas]
      @nombreAdmin = "Administrator "+ @nombre_antes.to_s
      @nombreDef = "Default"+ @nombre_antes.to_s

      @nombre = params[:area][:name]

      @existe = Area.find_by_name(@nombre.to_s)

      if @existe.nil?
        respond_to do |format|
          if @area.update(area_params)

            # @historic.detail = @area.to_json
            # @historic.save
            format.html {redirect_to areas_path, notice: t('activerecord.atributes.suc')}

            @belongs = params[:belongs]

            if @belongs == "1"
              @store = nil
            end

            if @belongs == "2"
              @store = "1"
            end

            # @area_agreagada = Area.find_by_id(@area.id)
            # @area_agreagada.store_id= @store
            # @area_agreagada.save

            @perfil = Profile.where("area_id = ?", @area.id)

            @perfil.each do |per|
              @pername = per.name

              if @pername == @nombreAdmin
                per.name = "Administrator " + @area.name
                per.flag = 1
                per.save

              elsif @pername == @nombreDef
                per.name = "Default" + @area.name
                per.flag = 2
                per.save

              else
                per.name = "PERFIL" + @area.name
                per.save
              end
            end

            format.json {head :no_content}
          else
            format.html {render action: 'edit'}
            format.json {render json: @area.errors, status: :unprocessable_entity}
          end
        end
      else
        redirect_to areas_path, alert: t('activerecord.atributes.not_updated')
      end

    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # DELETE /areas/1
  # DELETE /areas/1.json
  def destroy
    if current_user
      @page = "Areas"
      @user = current_user.name
      @user_id = current_user.id
      @userl = current_user.last_name
      @action = "4"
      @account = "not yet"
      @lasac = "not available"
      @cli_id = 0
      #@det = @loyalty_client.params[:id]
      @date = Time.now

      # @historic = Historic.new
      # @historic.page = @page
      # @historic.user_name = @user + " " + @userl
      # @historic.user_id = @user_id
      # #@historic.action = @det
      # @historic.action = @action
      # @historic.account = @account
      # @historic.last_account = @lasac
      # @historic.client_id = @cli_id
      # @historic.date = @date
      # #@historic.save

      @cu = current_user.profile_id

      if @cu != 0

        @per = Permission.where("view_name = 'Areas' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarArea = permisos.editar

          if permisos.view_name == @@byArea

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if current_user.habilitado == 0

          if ((@@eliminar == 1) && (@area.id != 1))

            @area.destroy

            respond_to do |format|
              # @historic.detail = @area.to_json
              # @historic.save

              if @area.destroy
                @perfiles = Profile.where("area_id = ? ", @area.id)
                if @perfiles.present?
                  @perfiles.each do |p|
                    @permisos = Permission.where("profile_id = ?", p.id)
                    if @permisos.present?
                      @permisos.each do |per|
                        per.destroy
                      end
                    end
                    p.destroy
                  end
                end

                @users = User.where("area_id = ?", @area.id)
                if @users.present?
                  @user.each do |u|
                    u.destroy
                  end
                end
              end
              format.html {redirect_to areas_url, :notice => t('activerecord.atributes.del')}
              #format.json {head :no_content}
            end

          else
            @Without_Permission = 100
            redirect_to home_index_path, :alert => t('all.not_access')
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_enabled')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_area
    @area = Area.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def area_params
    params.require(:area).permit(:name, :area_id)
  end
end
