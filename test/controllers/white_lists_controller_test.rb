require 'test_helper'

class WhiteListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @white_list = white_lists(:one)
  end

  test "should get index" do
    get white_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_white_list_url
    assert_response :success
  end

  test "should create white_list" do
    assert_difference('WhiteList.count') do
      post white_lists_url, params: { white_list: { Estado: @white_list.Estado, Fecha: @white_list.Fecha, IdTran: @white_list.IdTran, IdUsuario: @white_list.IdUsuario } }
    end

    assert_redirected_to white_list_url(WhiteList.last)
  end

  test "should show white_list" do
    get white_list_url(@white_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_white_list_url(@white_list)
    assert_response :success
  end

  test "should update white_list" do
    patch white_list_url(@white_list), params: { white_list: { Estado: @white_list.Estado, Fecha: @white_list.Fecha, IdTran: @white_list.IdTran, IdUsuario: @white_list.IdUsuario } }
    assert_redirected_to white_list_url(@white_list)
  end

  test "should destroy white_list" do
    assert_difference('WhiteList.count', -1) do
      delete white_list_url(@white_list)
    end

    assert_redirected_to white_lists_url
  end
end
