# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end

ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.irregular 'talertaxusr', 'talertaxusr'
  inflect.irregular 'taplicac', 'taplicac'
  inflect.irregular 'taplicaciones', 'taplicaciones'
  inflect.irregular 'tatendidasxhoy', 'tatendidasxhoy'
  inflect.irregular 'tbitacora', 'tbitacora'
  inflect.irregular 'tbitalarma', 'tbitalarma'
  inflect.irregular 'tbitaobse', 'tbitaobse'
  inflect.irregular 'tcamposlay', 'tcamposlay'
  inflect.irregular 'tcamposllave', 'tcamposllave'
  inflect.irregular 'tcamposxgrupo', 'tcamposxgrupo'
  inflect.irregular 'tequivalenciaxc', 'tequivalenciaxc'
  inflect.irregular 'testadosxalerta', 'testadosxalerta'
  inflect.irregular 'tformatos', 'tformatos'
  inflect.irregular 'tgrupos', 'tgrupos'
  inflect.irregular 'tlay', 'tlay'
  inflect.irregular 'tlaylogc', 'tlaylogc'
  inflect.irregular 'tperfiles', 'tperfiles'
  inflect.irregular 'ttiporegalarma', 'ttiporegalarma'
  inflect.irregular 'ttranalerta0101', 'ttranalerta0101'
  inflect.irregular 'txfilterxusr', 'txfilterxusr'
  inflect.irregular 'tx_filters', 'tx_filters'
  inflect.irregular 'tx_sessions', 'tx_sessions'
  inflect.irregular 'referenciadealerta', 'referenciadealerta'
  inflect.irregular 'taplicacioneskm', 'taplicacioneskm'
  inflect.irregular 'trcestatuses', 'trcestatus'
end
